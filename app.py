# -*- coding: utf-8 -*-
from flask import Flask, render_template, jsonify
#import final
import re
import os
from flask import request
import json
#from werkzeug import secure_filename
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
import nltk
from nltk.tokenize import sent_tokenize,word_tokenize
#nltk.download('punkt')
import root_pack
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
app = Flask(__name__)
flag=0
@app.route('/')
def index():
   return render_template('hello.html')


@app.route('/_add_numbers',  methods=['GET', 'POST'])
def _inputDATA_():
	print("entered to function")
	summary12=''
	if request.method == 'GET':
		url= request.args.get('url',0,type=str)
		html = urlopen(url)
		soup = BeautifulSoup(html, "lxml")
		a_text = soup.find_all('p')
		y=[re.sub(r'<.+?>',r'',str(a)) for a in a_text]
		h = [re.sub(r'[a-zA-Z]',r'',str(b)) for b in y]
		inputfile_path = "input.txt"
		with open(inputfile_path, "w", encoding="utf-8") as f:
			for text in h:
				f.write(text.strip())

		inputfile = open(inputfile_path, encoding = "utf8")
		contents = inputfile.read()
		inputfile.close()
		stopwordfile = open("stopwords.txt", encoding = "utf8")
		stopcontent = stopwordfile.read()
		stopwordfile.close()   
		scoreValue = _create_frequency_matrix(contents, stopcontent)
		threshold = _find_average_score(scoreValue)
		sentences=sent_tokenize(contents)
		
		summary12= _generate_Summary(sentences, scoreValue, 1.3 * threshold)
		final_sentences = sent_tokenize(summary12)
		print(final_sentences)
		summary_documents = len(final_sentences)
		print(summary_documents)
		output_file = "output.txt"
		f= open(output_file, "w", encoding="utf-8")
		f.write(summary12)
	return jsonify(summary12=summary12)


   
    #2 Removing
    	
    	
   
    	
    	
        	
        		
    	  
    	         	

#print(threshold)

# 9 Important Algorithm: Generate the summary
		
#print(summary)

		
		
		
#output_file_path = r"C:\Users\A1072702\Documents\Text Summarisation"
		

		
		
    	

def _create_frequency_matrix(contents, stopcontent):
	print(contents)
	punctuations = '''!()-[]{};:'",<>./?@#$%^&*_~'''
	sentences=sent_tokenize(contents)
	all_words=word_tokenize(contents)
	len_words = len(all_words)
	stopwords = word_tokenize(stopcontent)
	score = {}
	for sent in sentences:
		new_words = []
		freq_table = {}
		sent = ''.join([i for i in sent if not i.isdigit()])
		for x in sent: 
			if x in punctuations: 
				sent = sent.replace(x, "") 
		freq_table = {}
		words=word_tokenize(sent)
		for k in stopwords:
			for j in words:
				if k==j:
					words.remove(k)
		for wor in words:
			wordi = root_pack.root(wor)
			new_words.append(wordi)
		for w in new_words:
			if w in freq_table:
				freq_table[w] += 1
			else:
				freq_table[w] = 1  
		scoreValue = 0    
		for w in new_words:
			count = freq_table[w]
			scoreValue += count / len_words
		
		score[sent[:5]] = scoreValue  

	return score

#sentenceValue = scoreValue
def _find_average_score(sentenceValue):
	"""
	Find the average score from the sentence value dictionary
	:rtype: int
	"""
	sumValues = 0
	for entry in sentenceValue:
		sumValues += sentenceValue[entry]

	# Average value of a sentence from original summary_text
	average = (sumValues / len(sentenceValue))

	return average
def _generate_Summary(sentences, scoreValue, threshold):
	sentence_count = 0
	summary = ''

	for sentence in sentences:
		if sentence[:5] in scoreValue and scoreValue[sentence[:5]] >= (threshold):
			summary += " " + sentence
			sentence_count += 1 

	return summary
	#return jsonify(summary)	
	#print(summary)
#username = request.form.get('username')
##url=input("URL")

#url = "https://children.manoramaonline.com/padhippura/infrared-reveals-egyptian-mummies-hidden-tattoos.html"

#url = "https://www.mathrubhumi.com/news/kerala/koodathayi-sily-murder-case-second-chargesheet-filed-1.4451675"
#html = urlopen(url)
#soup = BeautifulSoup(html, "lxml") 
#print(soup)
# here, the "lxml" is the html parser





#a_text = soup.find_all('p')
#2 Removing
#y=[re.sub(r'<.+?>',r'',str(a)) for a in a_text]
#h = [re.sub(r'[a-zA-Z]',r'',str(b)) for b in y]

#inputfile_path = "input.txt"

#with open(inputfile_path, "w", encoding="utf-8") as f:
#	for text in h:
#		f.write(text.strip())

#inputfile = open(inputfile_path, encoding = "utf8")  
#contents = inputfile.read()         
#inputfile.close()   

 
	
#sentences = sent_tokenize(contents)
#total_documents = len(sentences)


# 2 Find the score for each sentence.

#return jsonify(f)	


if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0',port=5007)
