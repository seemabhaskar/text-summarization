#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 12:46:06 2020

@author: icfoss
"""

import nltk
from nltk.tokenize import sent_tokenize,word_tokenize
#nltk.download('punkt')
import root_pack
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re


def _create_frequency_matrix(contents, stopcontent):
    punctuations = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
    sentences=sent_tokenize(contents)
    all_words=word_tokenize(contents)
    len_words = len(all_words)
    stopwords = word_tokenize(stopcontent)
    score = {}
    for sent in sentences:
        new_words = []
        freq_table = {}
        sent = ''.join([i for i in sent if not i.isdigit()])
        for x in sent: 
            if x in punctuations: 
                sent = sent.replace(x, "") 
        freq_table = {}
        words=word_tokenize(sent)
        for k in stopwords:
            for j in words:
                if k==j:
                    words.remove(k)
        for wor in words:
            wordi = root_pack.root(wor)
            new_words.append(wordi)
        for w in new_words:
            if w in freq_table:
                freq_table[w] += 1
            else:
                freq_table[w] = 1  
        scoreValue = 0    
        for w in new_words:
            count = freq_table[w]
            scoreValue += count / len_words
        
        score[sent[:5]] = scoreValue  

    return score

#sentenceValue = scoreValue

def _find_average_score(sentenceValue) -> int:
    """
    Find the average score from the sentence value dictionary
    :rtype: int
    """
    sumValues = 0
    for entry in sentenceValue:
        sumValues += sentenceValue[entry]

    # Average value of a sentence from original summary_text
    average = (sumValues / len(sentenceValue))

    return average
        
def _generate_summary(sentences, scoreValue, threshold):
    sentence_count = 0
    summary = ''

    for sentence in sentences:
        if sentence[:5] in scoreValue and scoreValue[sentence[:5]] >= (threshold):
            summary += " " + sentence
            sentence_count += 1 

    return summary




inputfile_path = "input.txt"


inputfile = open(inputfile_path, encoding = "utf8")  
contents = inputfile.read()         
inputfile.close()   

stopwordfile = open("stopwords.txt", encoding = "utf8") 
stopcontent = stopwordfile.read()         
stopwordfile.close()    
    
sentences = sent_tokenize(contents)
total_documents = len(sentences)


# 2 Find the score for each sentence.
scoreValue = _create_frequency_matrix(contents, stopcontent)

# 8 Find the threshold
threshold = _find_average_score(scoreValue)
#print(threshold)

# 9 Important Algorithm: Generate the summary
summary = _generate_summary(sentences, scoreValue, 1.25 * threshold)
#print(summary)

final_sentences = sent_tokenize(summary)
summary_documents = len(final_sentences)
print(summary_documents)

#output_file_path = "Text_Summarisation"
output_file = "output.txt"

with open(output_file, "w", encoding="utf-8") as f:
    f.write(summary)


    
    
